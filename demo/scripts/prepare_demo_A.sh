#!/usr/bin/env sh

mkdir temp
cd temp

git checkout -b feature_A
git push -u origin feature_A
touch file_1.txt
git add .
git commit -m "Create file 1 on feature A"
git push

git checkout -b feature_B
git push -u origin feature_B
touch file_4.txt
git add .
git commit -m "Create file 4 on feature B"
git push

git checkout feature_A
touch file_2.txt
git add .
git commit -m "Create file 2 on feature A"
git push

git checkout feature_B
touch file_5.txt
git add .
git commit -m "Create file 5 on feature B"
git push

git checkout feature_A
touch file_3.txt
git add .
git commit -m "Create file 3 on feature A"
git push

git checkout feature_B
touch file_6.txt
git add .
git commit -m "Create file 6 on feature B"
git push