# GIT Memo


## Table of contents
1. [Good practice](#good-practice)
2. [Branches model and git architecture](#branches-model-and-git-architecture)
3. [The basic](#the-basic)
    1. [Init/clone repository](#initclone-repository)
    2. [Check the state of our local repository](#check-the-state-of-our-local-repository)
    3. [Change the current working branch](#change-the-current-working-branch)
    4. [Update our local branches definition](#update-our-local-branches-definition)
    5. [Create a new branch](#create-a-new-branch)
    6. [Merge a branch](#merge-a-branch)
    7. [Commit some code](#commit-some-code)
    8. [Modify the previous commit](#modify-the-previous-commit)
    9. [Push our code/new branch to the remote repository](#push-our-codenew-branch-to-the-remote-repository)
    10. [Keep our branch updated : get the remote repository changes](#keep-our-branch-updated-get-the-remote-repository-changes)
    11. [Cancel our modifications (back to last commit)](#cancel-my-modifications-back-to-last-commit)
    12. [Record our changes and restore them](#record-our-changes-and-restore-them)
    13. [Create a Tag](#create-a-tag)
4. [Advanced use](#advanced-use)
    1. [Revert the state of our branche (undo changes and commits)](#revert-the-state-of-our-branche-undo-changes-and-commits)
    2. [Rebase our current branch onto another branch](#rebase-our-current-branch-onto-another-branch)
    3. [Rebase our code when pulling changes of the current branch](#rebase-our-code-when-pulling-changes-of-the-current-branch)
    4. [Use Git flow](#use-git-flow)



## Good practice

- Never push directly on the master branch
- Never push directly on the current development branch
- Always create your own branch when working on a feature
- Rebase your branch before merge it
- Always use merge request (or pull request for github) to ask to merge your branch on the current development (or feature) branch
- Be sure to understand what you do


## Branches model and git architecture

[See here](https://nvie.com/posts/a-successful-git-branching-model/)


## The basic

### Init/clone repository

Init new project :
```
cd /path/to/your/project
git init
```

Clone existing project
```
git clone <repo_URL>
```

### Check the state of our local repository

```
git status
```

### Change the current working branch

By default, we are on the 'master' branch.


To swith to another branch, we can do :
```
git checkout branch-name
```

### Update our local branches definition

In order to keep our local repository updated with the remote repository, we can update our list of local branches :

```
git fetch --all
```

### Create a new branch

To create a new branch and making it the current working branch
```
git branch new-branch
git checkout new branch
```

You can do the same with only one command :
```
git checkout -b new-branch
```

### Merge a branch

To merge 'some_fixes' branch into 'feature' branch :
```
git checkout feature
git merge some_fixes
```

### Commit some code

**Code is not send to the remote repository. Our commit is not visible by other people until we push the commit**

```
git add new-file
git commit -m "Here, we commit new-file to our local repo"
```


To commit all our changes :

```
git add .
git commit -m "Here, we commit all our modifications to our local repo"
```

### Modify the previous commit

We can modify the previous commit (change the content, or the commit message) with :
```
git commit --amend
```

Or if we only want to change the content without modify the commit message :
```
git commit --amend --no-edit
```

### Push our code/new branch to the remote repository

To push our local 'mybranch' branch to the remote repository :
```
git push origin mybranch
```

### Keep our branch updated : get the remote repository changes

To push our local 'mybranch' branch to the remote repository :
```
git pull
```

### Cancel our modifications (back to last commit)

To cancel our changes (equivalent of **svn revert**) to go back to our last commit state :
```
git reset HEAD
```

### Record our changes and restore them

To save our current changes and revert to the last commit
```
git stash
```

To reapply the saved changes 
```
git stash apply
```

Or, if you want reapply the changes and delete the record
```
git stash pop
```

### Manage tags

To list all avilable tags
```
git tag
```

To create a new tag
```
git tag -a v1.0 -m "Version 1.0"
```

Send the tag to the remote repository
```
git push origin v1.0
```




## Advanced use


### Revert the state of our branche (undo changes and commits)

**This command is not the equivalent of svn revert. Be carefull.**

With the revert command, we can undo all our current not commited changes, **and all our not pushed commits**.
```
git revert
```

### Rebase our current branch onto another branch

To rebase the 'feature' branch onto 'dev' branch :
```
git checkout feature
git rebase dev
```

Then, the next time you will push your code to the remote repository, you will have to force the push in order to rewrite the git history.
```
git push --force
```

### Rebase our code when pulling changes of the current branch

To get all the remote changes of the current branch, and keep my commits at the end of the branche
```
git pull --rebase
```

Then, the next time you will push your code to the remote repository, you will have to force the push in order to rewrite the git history.
```
git push --force
```

### Use git flow

Initialize Git flow in a project
```
git flow init
```

To start a new development branch.
```
git flow feature start dev_branch_name
```
is equivalent to
```
git checkout -b dev_branch_name dev
```

To finish an existing development branch.
```
git flow feature finish dev_branch_name
```
is equivalent to
```
git checkout dev
git merge dev_branch_name --no-ff
git branch -d dev_branch_name
```


You can do the same with releases and hotfixes.
```
git flow release start release_branch_name
git flow release finish release_branch_name
git flow hotfix start release_branch_name
git flow hotfix finish release_branch_name
```

For release and hotfixes, the equivalent are
```
git checkout -b new_branch_name master
```
and
```
git checkout dev
git merge new_branch_name --no-ff
git checkout master
git merge new_branch_name --no-ff
git tag v1.0
git branch -d dev_branch_name
```
